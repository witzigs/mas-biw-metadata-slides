+++
outputs = ["Reveal"]
+++
<style>
.container{
    display: flex;
}
.col{
    flex: 1;
}
</style>

# Bibliothekarische Metadatenformate: Datenaustausch
  
---

## Intro
* Datenaustausch
* Schnittstellen (OAI-PMH, SRU)
* Ausblick Linked Data