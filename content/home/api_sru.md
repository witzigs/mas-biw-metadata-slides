+++
weight = 320
+++
{{% section %}}

## SRU

---

### Intro
* **S**earch/**R**etrieval via **U**RL
* Protokoll für Suchanfragen im Internet mittels CQL (**C**ontextual **Q**uery **L**anguage)
* Spezifikationen: https://www.loc.gov/standards/sru/
* Offizielle Weiterentwicklung von Z39.50 (ab 2004)
* Version 1.1 (2004), **1.2** (2007), 2.0 (2013)

---

### Anwendungsbeispiele
* Meta-Suchportale wie KVK, Archives Online
* Suche im Katalog über Literaturverwaltungssysteme
* Fremddatenübernahme

---

### Grundprinzipien
* Server: stellt SRU für die Suche zur Verfügung
* Client: schickt Suchanfragen an den Server, nimmt Antworten entgegen und bereitet sie auf
* CQL für Suchanfragen
* Basiert auf XML/HTTP (REST)

---

### [Parameter und Query](https://witzigs.gitlab.io/mas-biw-metadata-docs/schnittstellen/sru/requests/)

{{% /section %}}

---

### OAI oder SRU?

|OAI|SRU|
|---|---|
|Harvesting, einmalig oder regelmässig|Suche in externer Quelle und Darstellung in eigenem Kontext|
|Abholen von Updates|Adhoc Abfrage, die immer alle Daten zurückliefert|
|Rudimentäre Selektionskriterien (sets, datestamp)|Ausgefeilte Suchmöglichkeiten (CQL)|