+++
weight = 340
+++

## Weitere Schnittstellen

---

### Z39.50
* Netzwerkprotokoll zur Abfrage von Bibliothekssystemen (nicht HTTP!)
* Nur im Bibliothekswesen genutzt
* Entwickelt ab ca. 1980
* Vorgänger von SRU
* Nutzung wie SRU, Suchabfragen auf Server (z.B. Suche in Fremddaten)
* Beispiel: [Alma Z39.50](https://developers.exlibrisgroup.com/alma/integrations/z39-50/)

---

### Systemspezifische Schnittstellen
* Schnittstellen basierend auf Prinzipien von REST, WebHooks, ...
* Nur innerhalb eines Systems standardisiert
* Können auch schreibenden Zugriff ermöglichen
* Ermöglichen Automatisierung von Aufgaben, Anbindung von Drittsystemen, Verändern von Daten, ...
* Beispiel: [Alma REST APIs](https://developers.exlibrisgroup.com/alma/apis/)

---

### Datenaustausch mit SLSP (Alma)
* [Export](https://knowledge.exlibrisgroup.com/Alma/Product_Documentation/010Alma_Online_Help_(English)/090Integrations_with_External_Systems/030Resource_Management/020Exporting_Metadata) von Daten mit Job "Export Bibliographic records"
* [OAI](https://developers.exlibrisgroup.com/alma/integrations/oai/): Zugriff über IP freischalten
* [SRU](https://developers.exlibrisgroup.com/alma/integrations/sru/) und [Z39.50](https://developers.exlibrisgroup.com/alma/integrations/z39-50/): freier Zugriff auf NZ und IZs
* [REST APIs](https://developers.exlibrisgroup.com/alma/apis/): für Entwickler, Zugriff freischalten (schreibend!)

---

## Schnittstellen in der Praxis

---

### Gibt es eine Schnittstelle?
* Schnittstellen sind im Funktionsumfang des Bibliothekssystems enthalten oder nicht
* Dokumentation des Bibliothekssystems oder des Service
* Nachfragen
* Entwicklung anstossen
* Evtl. Eigenentwicklung (eher Kontext Service als Bibliothekssystem)

---

### Daten über Schnittstellen beziehen
* Skripte, z.B. in Python
    * [Jupyter Notebook Tutorials der DNB](https://www.dnb.de/DE/Professionell/Services/WissenschaftundForschung/DNBLab/dnblabTutorials.html?nn=849628#doc1038762bodyText2)
    * [Python Digital Toolbox Jupyter Notebooks der UB Bern](https://github.com/ub-unibe-ch/ds-pytools)
* Software mit enthaltenem OAI- und/oder SRU-Client (z.B. [Catmandu](https://librecat.org/index.html), [MarcEdit](https://marcedit.reeset.net/), ...)
