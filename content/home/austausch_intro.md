+++
weight = 200
+++
{{% section %}}
## Austausch von Metadaten

---

#### Basis für die tägliche Arbeit
*Fallbeispiel Nationalbibliothek*

<img class="special-img-class" style="width:60% ; border:none" src="/images/Austauschformate_NB.png" />

---

#### Basis für die tägliche Arbeit
* Fremddatenübernahme bei der Katalogisierung 
  * Rolle als Datennehmerin und als Datengeberin
  * Datenquellen: andere Bibliotheken, Verlage, Buchhandel, ...
* Bezug von Metadaten für E-Medien Pakete
* Anbindung von Normdaten
* Kataloganreicherung

---

#### Basis für Services
*Fallbeispiel ZB Zürich*

<img class="special-img-class" style="width:50% ; border:none ; margin-top:2px ; margin-bottom:2px" src="/images/data-map-der-zentralbibliothek-zurich.jpg" /> <br/> <a href="https://data.zb.uzh.ch/map/books/data-map-der-zentralbibliothek-zurich/page/systemubersicht" style="font-size:40%">Data Map der ZB Zürich (Stand: 11.12.2023)</a>

---

#### Basis für Services
*Fallbeispiel swisscollections als Datennehmerin und -geberin*

<img class="special-img-class" style="width:80% ; border:none" src="/images/swisscollections-api.png" />

---

### Basis für Services

* Services ergänzend zum Bibliothekssystem und Discovery
* Meta-Suche über verschiedene Datenquellen
* Datennutzung durch Forschung, Projekte, ...

---

### Komponenten

* Austauschformate
* Schnittstellen
* Technische Infrastruktur

{{% /section %}}
