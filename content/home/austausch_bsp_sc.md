+++
weight=330
draft=true
+++

{{% section %}}

## Fallbeispiel swisscollections

---

### swisscollections
* Sucheinstieg zu historischen und modernen Sammlungen in Schweizer Bibliotheken und Archiven
* Ergänzung zu swisscovery für eine spezialisierte und differenzierte Suche in Spezialbeständen
* Verein swisscollections
* Entwicklung durch die IT der UB Basel


---

### OAI und SRU in swisscollections

<img class="special-img-class" style="width:80% ; border:none" src="/images/swisscollections-api.png" />

|[swisscollections](https://swisscollections.ch/)|[swisscollections SRU für AO](https://sru.swisscollections.ch/ao?version=1.2&operation=explain)|[Archives Online](https://www.archives-online.org/Search)|
|---|---|---|

{{% /section %}}
