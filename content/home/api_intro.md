+++
weight = 300
+++
## Wie werden Daten ausgetauscht?

---

### Datendump
* Einfachste Möglichkeit für Datenaustausch
* Geeignet für einmaligen Datenaustausch
* Weniger Programmierkenntnisse zum Abholen und zur Verfügung stellen
* Umgang mit grossen Datenmengen
* Handarbeit

---

### Schnittstellen
* API **A**pplication **p**rogramming **i**nterface
* Automatisierter Datenaustausch
* Anwendung/Service holt Daten für ihre Zwecke ab und/oder sendet Daten ins System zurück
* Eine Seite initialisiert Prozess:
    - Client-Server Prinzip: Server stellt Daten zur Verfügung, Client fragt Daten ab
    - WebHook: Server sendet Daten, Client empfängt

---

### RESTful API
* **Re**presentational **S**tate **T**ransfer
* Basis für Webservices wie Schnittstellen
* Befehl von Client wird über HTTP als Teil der Query einer URL an Server geschickt
* Der Server verarbeitet den Befehl und schickt Daten zurück
* Protokolle legen die Form der Query und der zurückgesendeten Daten fest

https://<span>www.google.<span>com/search?**hl=en&q=kakapo+and+kiwi**

---

### Schnittstellen im Bibliotheksbereich
* OAI-PMH
* SRU
* Z39.50
* Systemspezifische Schnittstellen
