+++
weight = 400
+++

## Datenaustausch in Zukunft

---

### Linked Data im MAS

* IFLA LRM (Library Reference Model) und RDA -> Modul 4 Erschliessung
* LOD und Semantic Web -> Modul 5 Digitalisierung und Datenmanagement

---

### Linked Data

<img class="special-img-class" style="width:100% ; border:none" src="/images/bibresource-jane-letters_nur-sb.png" />

---

### Linked Data: Wieso?

* Vernetzung mit anderen Datensets
* Anreicherung eigener Daten
* Andere Suchmöglichkeiten (Welche Werke wurden im 19. Jhr. von Schweizerinnen verfasst?)
* Nutzung von Bibliotheksdaten in anderen Kontexten

---

### Linked Open Data Cloud

<img class="special-img-class" style="width:50% ; border:none" src="/images/lod-cloud.jpeg" />

[Linked Open Data Cloud](https://lod-cloud.net/)

---

### Linked Data im Bibliotheksbereich

Diskutiert wird...

* ... die Publikation von LOD für die Nutzung ausserhalb Bibliothek
* ... die Nutzung von LOD, z.B. für Suchoberflächen
* ... die Ablösung von MARC21 als Austauschformat
* ... die Ablösung von MARC21 als Erschliessungsformat

---

### Linked Data publizieren

* Entscheid für ein Datenmodell
* Transformation der MARC-Daten
* SPARQL oder REST API für die Nutzung dieser Daten
* Beispiel: GND über lobid ([lobid-gnd](https://lobid.org/gnd))

---

{{% section %}}

### IFLA LRM

* Datenmodell bibliografisches Universum
* Beschreibung von Entitäten
* Beziehungen zwischen Entitäten
* Basis für RDA

---

### IFLA LRM

<img class="special-img-class" style="width:80% ; border:none" src="/images/ifla-lrm.png" />

{{% /section %}}

---

{{% section %}}

### BIBFRAME
* **Bib**liographic **Frame**work Initiative
* Ziele: Ablösung von MARC, dabei robustes Austauschformat erhalten
* Diverse Aspekte einbezogen: Datenaustausch, auch ausserhalb der Bibliotheken, Tools und Prozesse für eine Umstellung von MARC zu BIBFRAME, Methoden für die Erschliessung, ...


<img class="special-img-class" style="width:15%;border:none" src="/images/bibframe-logo-small.jpeg" />

---

### BIBFRAME
* Datenmodell und Vokabular nach RDF
* BIBFRAME 1.0 2012, 2016 überarbeitet zu BIBFRAME 2.0
* Entwicklung und Einsatz in einzelnen Bibliotheken
* Kontinuierliche Entwicklung Vokabular über [GitHub](https://github.com/lcnetdev/bibframe-ontology)
* BIBFRAME Interoperability Group ([BIG](https://wiki.lyrasis.org/pages/viewpage.action?pageId=249135298
  )) seit 2022

---

### [BIBFRAME Modell](https://www.loc.gov/bibframe/docs/bibframe2-model.html)

![Bibframe model](/images/bf2-model.jpeg "image")

---

### BIBFRAME in der Praxis

* Library of Congress: Erschliessung und Transformation für Austausch
* Erschliessung in einem auf BIBFRAME basierenden Modell (Libris (Schweden), RERO+, ...)
* Alma: Transformation zu BIBFRAME, Erschliessung in Entwicklung
* Internationale Zusammenarbeit für Interoperabilität

---

### BIBFRAME Beispiel

[Compare MARC converted to BIBFRAME](https://id.loc.gov/tools/bibframe/compare-id/full-ttl)

[JSON Export aus RERO+](https://bib.rero.ch/global/documents/520665)


{{% /section %}}